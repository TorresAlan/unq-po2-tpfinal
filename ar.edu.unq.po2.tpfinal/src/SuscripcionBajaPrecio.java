package src;

/**
 * Interfaz para el observador de baja de precio de un inmueble.
 * @author AlanTorres
 *
 */

public interface SuscripcionBajaPrecio {
	
	public void updateBajaPrecio(String tipoInmueble, float precio);

}
