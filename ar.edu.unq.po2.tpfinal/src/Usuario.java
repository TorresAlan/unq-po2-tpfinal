package src;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Usuario {
	
	private String nombre;
	private String email;
	private int telefono;
	private String contrasenia;
	
	private Sistema sistema;
	
	private ArrayList<Inmueble> misInmueblesPublicados;
	
	private Ranking rInquilino;
	private Ranking rPropietario;
	private ArrayList<Reserva> misReservasRealizadas;
	private ArrayList<Reserva> solicitudesDeReserva;
	
	public Usuario(String nombre, String email, int telefono, String contrasenia, Sistema sistema) {
		
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
		this.contrasenia = contrasenia;
		this.sistema = sistema;
		this.rInquilino = new Ranking(this.sistema.getCategoriaInquilino());
		this.rPropietario = new Ranking(this.sistema.getCategoriaPropietario());
		this.misReservasRealizadas = new ArrayList<Reserva>();
		this.solicitudesDeReserva = new ArrayList<Reserva>();
		this.misInmueblesPublicados = new ArrayList<Inmueble>();
	}
	
	public ArrayList<Reserva> getSolicitudesDeReserva() {
		return this.solicitudesDeReserva;
	}
	
	public ArrayList<Reserva> getMisReservas() {
		return this.misReservasRealizadas;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getContrasenia() {
		return this.contrasenia;
	}
	
	public int getTelefono() {
		return this.telefono;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Ranking getRankingPropietario() {
		return this.rPropietario;
	}
	
	public Ranking getRankingInquilino() {
		return this.rInquilino;
	}
	
	public ArrayList<Inmueble> getMisInmueblesPublicados() {
		return this.misInmueblesPublicados;
	}
	
/**
 * Crea un nuevo inmueble y lo agrega al sistema
 * En caso de que el tipoInmueble o los servicios no sean aceptados por el sistema devuelve un error.
 */
	public void publicarInmueble(String tipoInmueble, float superficie,String pais,
									String ciudad,String direccion,ArrayList<String>servicios,
									int capacidad,ArrayList<String>fotos, LocalTime checkIn,
									LocalTime checkOut, ArrayList<String> formasDePago,
									float precio) {
		
		if(this.sistema.getServicios().containsAll(servicios) && this.sistema.getTipoInmueble().contains(tipoInmueble)){
			Inmueble inmueble = new Inmueble (tipoInmueble, superficie, pais, ciudad, direccion, servicios, capacidad, fotos, this, checkIn, checkOut, formasDePago, precio);
			this.sistema.getPublicaciones().add(inmueble);
			this.misInmueblesPublicados.add(inmueble);
		}
		else {
			throw new IllegalArgumentException("Tipo de inmueble o servicio no valido");
		}
	}

/**
 * 	Realiza la reserva del inmueble dado.
 *  Si el inmueble esta ocupado en la fecha dada o la forma de pago no es aceptada por el propietario devuelve un error.
 */
	public void realizarReserva(Inmueble inmueble, LocalDate inicio, LocalDate fin, String formaDePago, String detallesDelPago) {
		
		if(!inmueble.estaOcupado(inicio, fin) || inmueble.getFormasDePago().contains(formaDePago)) {
			Reserva reserva = new Reserva(this, inmueble, inicio, fin, detallesDelPago);
			this.misReservasRealizadas.add(reserva);
		}else {
			throw new IllegalArgumentException("Inmueble ocupado o forma de pago invalida");
		}
		
	}

	protected Sistema getSistema() {
		return this.sistema;
	}

/**
 * 	Dado un inmueble y unas fechas en las que el inmueble este ocupado realiza una reserva condicional.
 */
	public void realizarReservaCondicional(Inmueble inmueble, LocalDate inicio, LocalDate fin, String formaDePago, String detallesDelPago) {
		
		Reserva reserva = inmueble.buscarReserva(inicio, fin);
		reserva.getReservaCondicional().add(new ReservaCondicional(this, inmueble, inicio, fin, formaDePago, detallesDelPago));
		
	}

/**
 * 	Dada una reserva con el checkOut ya realizado y un ranking realiza un nuevo rankeo.
 *  
 */
	public void rankear(Reserva reserva, Ranking ranking, String categoria, int nota, String comentario) {
		
		if(reserva.seRealizoElCheckOut()) {
			ranking.rankear(categoria, nota, comentario, this);
		} else {
			throw new IllegalArgumentException("Aun no se realizo el checkOut");
		}
	}
	
	
	
}

