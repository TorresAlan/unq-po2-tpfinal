package src;

/**
 * Interfaz externa dada en el enunciado
 */

public interface HomePagePublisher {
	
	public void publish(String message);

}
