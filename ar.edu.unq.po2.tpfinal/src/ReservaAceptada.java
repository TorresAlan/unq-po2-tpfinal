package src;

public class ReservaAceptada implements EstadoDeReserva {
	
	private Reserva reserva;
	
	public ReservaAceptada(Reserva reserva) {
		this.reserva = reserva;
	}
 
/**
 * Envia un error al querer aceptar una reserva ya aceptada.
 */
	@Override
	public void aceptarReserva(Usuario usuario) {
		throw new IllegalArgumentException("Esta reserva ya a sido aceptada");
		
	}

/**
 * Envia al inquilino el monto a pagar segun la politica de cancelacion y en caso de haber reservas condicionales realiza la reserva.
 * Si hay reservas condicionales en cola realiza la reserva del primer elemento que la queue.
 * Si no hay reservas condicionales ejecita la notificacion de cancelacion.
 */
	@Override
	public void cancelarReserva() {
		// TODO cosas con la politica de cancelacion
		if(!this.reserva.getReservaCondicional().isEmpty()) {
			this.reserva.getReservaCondicional().poll().realizarReserva(reserva);
		}
		else {
			this.reserva.getInmueble().getNotificador().notificarCancelacion(this.reserva.getInmueble().getTipoInmueble());
		}
		reserva.getInquilino().getMisReservas().remove(reserva);
		reserva.getInquilino().getSistema().getReservas().remove(reserva);
	}

	
	
	
}
