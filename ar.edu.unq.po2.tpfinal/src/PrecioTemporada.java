package src;

import java.time.LocalDate;

public class PrecioTemporada {
	
	private LocalDate inicio;
	private LocalDate fin;
	private float precio;

	public PrecioTemporada(LocalDate inicio, LocalDate fin, float precio) {
		this.inicio = inicio;
		this.fin = fin;
		this.precio = precio;
	}

	public float getPrecio() {
		return precio;
	}

/**
 * Dada una fecha, devuelve true si pertenece a la temporada.
 */
	public boolean perteneceFecha(LocalDate fecha) {
		return (this.inicio.isBefore(fecha) || this.inicio.isEqual(fecha)) && (this.fin.isAfter(fecha) || this.fin.isEqual(fecha));
	}

}
