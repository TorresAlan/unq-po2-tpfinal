package src;

import java.time.LocalDate;
import java.util.ArrayList;

public class BuscarPrecioMayor extends BuscarInmueble{
	
	private float precioMayor;

	public BuscarPrecioMayor(String ciudad, LocalDate inicio, LocalDate fin, ArrayList<Inmueble> publicaciones, float precioMayor) {
		super(ciudad, inicio, fin, publicaciones);
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<Inmueble> buscar(){
		ArrayList<Inmueble> resultado= new ArrayList<>();
		this.getPublicaciones().parallelStream().filter(p -> !p.estaOcupado(inicio, fin) && this.esMismaCiudad(ciudad, p.getCiudad()) && this.esMenorA(this.getPrecioMayor(), p.getPrecio()) ).forEach(j -> resultado.add(j));	
		return resultado;
	}
	
	private float getPrecioMayor() {
		// TODO Auto-generated method stub
		return precioMayor;
	}

	public boolean esMenorA(float num1, float num2) {
		return num1>num2;
		
		
	}

}
