package src;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class BusquedaCompuesta extends BuscarInmueble{
	
	private ArrayList<BuscarInmueble> busqueda;

	public BusquedaCompuesta(String ciudad, LocalDate inicio, LocalDate fin, ArrayList<Inmueble> publicaciones,ArrayList<BuscarInmueble> busqueda) {
		super(ciudad, inicio, fin, publicaciones);
		this.busqueda = busqueda;
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<Inmueble> buscar(){
		ArrayList<Inmueble> resultado= new ArrayList<>();
		this.getBusqueda().parallelStream().forEach(p -> resultado.addAll(p.buscar()));	
	    Set<Inmueble> hashSet = new HashSet<Inmueble>(resultado);
	    resultado.clear();
	    resultado.addAll(hashSet);
			
		return resultado;
	}
	
	public ArrayList<BuscarInmueble> getBusqueda(){
		return busqueda;
	}
	

}
