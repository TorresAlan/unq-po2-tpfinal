package src;

import java.time.LocalDate;
import java.util.ArrayList;


public abstract class BuscarInmueble {
	
	protected String ciudad;
	protected LocalDate inicio;
	protected LocalDate fin;
	private ArrayList<Inmueble> publicaciones;
	
	public BuscarInmueble(String ciudad,LocalDate inicio,LocalDate fin,ArrayList<Inmueble> publicaciones){	
		this.ciudad = ciudad;
		this.inicio = inicio;
		this.fin = fin;
		this.publicaciones = publicaciones;
	}
	

	public ArrayList<Inmueble> buscar(){
		ArrayList<Inmueble> resultado= new ArrayList<>();
		this.getPublicaciones().parallelStream().filter(p -> !p.estaOcupado(inicio, fin) && this.esMismaCiudad(ciudad, p.getCiudad()) ).forEach(j -> resultado.add(j));	
		return resultado;
	}
	
	
	
	public boolean esMismaCiudad(String ciudad1, String ciudad2){
		return ciudad1 == ciudad2;
		
	}
	
	public String getCiudad() {
		return ciudad;
	}

	public ArrayList<Inmueble> getPublicaciones(){
		return publicaciones;
	}
	
	public boolean esFechaMayor(LocalDate fecha1, LocalDate fecha2){
		return fecha1.compareTo(fecha2) < 0;
	}

}
