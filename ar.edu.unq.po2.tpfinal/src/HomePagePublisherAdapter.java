package src;

/**
 * Adaptador creado para que la interfaz externa funcione con el Notificador.
 */

public class HomePagePublisherAdapter implements SuscripcionBajaPrecio {
	
	private HomePagePublisher suscripcion;
	
	public HomePagePublisherAdapter(HomePagePublisher suscripcion) {
		
		this.suscripcion = suscripcion;
		
	}

/**
 * Adapta el metodo del notificador al metodo de la interfaz usada por el objeto externo.
 */
	@Override
	public void updateBajaPrecio(String tipoInmueble, float precio) {
		
		suscripcion.publish("No te pierdas esta oferta: Un inmueble " + tipoInmueble + " a tan solo " + precio + " pesos");
		
	}



}
