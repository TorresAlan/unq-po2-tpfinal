package src;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Queue;

public class Reserva {
	
	private EstadoDeReserva estado;
	private Usuario inquilino;
	private Inmueble publicacion;
	private LocalDateTime inicio;
	private LocalDateTime fin;
	private String detallesDePago;
	private Queue<ReservaCondicional> reservaCondicional;
	private float precioTotal;
	
	protected Reserva(Usuario inquilino, Inmueble publicacion, LocalDate inicio, LocalDate fin, String detallesDePago) {
		
		this.inquilino = inquilino;
		this.publicacion = publicacion;
		this.inicio = LocalDateTime.of(inicio, this.publicacion.getCheckIn());
		this.fin = LocalDateTime.of(fin, this.publicacion.getCheckOut());
		this.reservaCondicional = new LinkedList<ReservaCondicional>();
		this.detallesDePago = detallesDePago;
		
		this.estado = new ReservaPendiente(this);
		
		this.publicacion.getPropietario().getSolicitudesDeReserva().add(this);
		this.precioTotal = this.publicacion.precioDeHasta(inicio, fin);
	}
	
	public Inmueble getInmueble() {
		return this.publicacion;
	}
	
	public String getDetallesDePago() {
		return this.detallesDePago;
	}
	
	public float getPrecioTotal() {
		return this.precioTotal;
	}

/**
 * 	Utiliza el metodo aceptar reserva del estado actual.
 */
	public void aceptarReserva(Usuario usuario) {
		this.estado.aceptarReserva(usuario);
	}

/**
 * Utiliza el metodo cancelar reserva del estado actual.	
 */
	public void cancelarReserva() {
		this.estado.cancelarReserva();
	}

	public Usuario getInquilino() {
		return this.inquilino;
	}

	protected void setEstado(EstadoDeReserva estado) {
		this.estado = estado;
	}
	
	protected Queue<ReservaCondicional> getReservaCondicional(){
		return this.reservaCondicional;
	}
	
	protected LocalDate getInicio() {
		return this.inicio.toLocalDate();
	}
	
	protected LocalDate getFin() {
		return this.fin.toLocalDate();
	}

	protected void setReservaCondicional(Queue<ReservaCondicional> reservasCondicional) {
		this.reservaCondicional = reservasCondicional;
	}

/**
 * Devuelve True si se realizo el checkOut de la reserva	
 */
	protected boolean seRealizoElCheckOut() {
		return this.inicio.isBefore(LocalDateTime.now());
	}
	
	
	
	

}
