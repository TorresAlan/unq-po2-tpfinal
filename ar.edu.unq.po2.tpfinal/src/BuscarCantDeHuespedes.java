package src;

import java.time.LocalDate;
import java.util.ArrayList;

public class BuscarCantDeHuespedes extends BuscarInmueble {
	
	private int cantDeHuesped;

	public BuscarCantDeHuespedes(String ciudad, LocalDate inicio, LocalDate fin,
			ArrayList<Inmueble> publicaciones,int cantDeHuesped) {
		super(ciudad, inicio, fin, publicaciones);
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<Inmueble> buscar(){
		ArrayList<Inmueble> resultado= new ArrayList<>();
		this.getPublicaciones().parallelStream().filter(p -> !p.estaOcupado(inicio, fin) && this.esMismaCiudad(ciudad, p.getCiudad()) && this.supereCapasidad(this.getCantDeHuesped(), p.getCapacidad()) ).forEach(j -> resultado.add(j));	
		return resultado;
	}
	
	private int getCantDeHuesped() {
		// TODO Auto-generated method stub
		return cantDeHuesped;
	}

	public boolean supereCapasidad(int num1, int num2) {
		return num1 > num2;
		
		
	}
	

	
}
