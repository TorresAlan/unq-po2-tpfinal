package src;

import java.time.LocalDate;
import java.util.Queue;

public class ReservaCondicional {
	
	private Usuario inquilino;
	private Inmueble publicacion;
	private LocalDate inicio;
	private LocalDate fin;
	private String formaDePago;
	private String detallesDePago;
	
	public ReservaCondicional (Usuario inquilino, Inmueble publicacion, LocalDate inicio, LocalDate fin, String formaDePago, String detallesDePago) {
		
		this.inquilino = inquilino;
		this.publicacion = publicacion;
		this.inicio = inicio;
		this.fin = fin;
		this.formaDePago = formaDePago;
		this.detallesDePago = detallesDePago;
		
	}

/**
 * 	Realiza una nueva reserva de la misma forma que la haria un inquilino.
 * 	Pone el resto de la queue de la reserva original en la nueva.
 */
	protected void realizarReserva(Reserva reserva) {
		
		Queue<ReservaCondicional> reservasCondicional = reserva.getReservaCondicional();
		this.inquilino.realizarReserva(publicacion, inicio, fin, formaDePago, detallesDePago);
		this.inquilino.getMisReservas().get(this.inquilino.getMisReservas().size() - 1).setReservaCondicional(reservasCondicional);
	}
	

	
	
	
	
}
