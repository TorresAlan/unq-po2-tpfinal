package src;

public class Administrador {
	
	private Sistema sistema;
	
	public Administrador(Sistema sistema) {
		
		this.sistema = sistema;
	}
	
/**
 * Dado un tipoDeInmueble lo agrega a los tipos de inmueble validos que podra utilizar el sistema.
 */
	public void darDeAltaTipoDeInmueble(String tipoInmueble) {
		this.sistema.getTipoInmueble().add(tipoInmueble);
	}

/**
 * 	Dado un servicio lo agrega a los servicios validos que podra utilizar el sistema.
 */
	public void darDeAltaServicios(String servicio) {
		this.sistema.getServicios().add(servicio);
	}
	
/**
 * Dada una categoria la agrega a las categorias validas para rankear un inquilino.
 */
	public void darDeAltaCategoriaInquilino(String categoria) {
		this.sistema.getCategoriaInquilino().add(categoria);
	}

/**
 * Dada una categoria la agrega a las categorias validas para rankear un propietario.
 */
	public void darDeAltaCategoriaPropietario(String categoria) {
		this.sistema.getCategoriaPropietario().add(categoria);
	}
	
/**
 * Dada una categoria la agrega a las categorias validas para rankear un inmueble.
 */
	public void darDeAltaCategoriaInmueble(String categoria) {
		this.sistema.getCategoriaInmueble().add(categoria);
	}

}
