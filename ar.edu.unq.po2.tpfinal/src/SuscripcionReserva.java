package src;

import java.time.LocalDate;

/**
 * Interfaz para el observador de reserva de un inmueble.
 * @author Alan Torres
 *
 */

public interface SuscripcionReserva {
	
	public void updateReserva(String tipoInmueble, LocalDate inicio, LocalDate fin);

}
