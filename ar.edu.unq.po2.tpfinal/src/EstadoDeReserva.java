package src;

/**
 * Interfaz usada por el estado de las reservas.
 *
 */
public interface EstadoDeReserva {
	
	public void aceptarReserva(Usuario usuario);
	public void cancelarReserva();

}
