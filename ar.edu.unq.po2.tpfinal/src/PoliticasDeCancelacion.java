package src;

import java.time.LocalDate;
import java.time.Period;

public abstract class PoliticasDeCancelacion {
	
	public float cancelar(Reserva reserva) {
		return 0;
	}
	
	public boolean esCancelacionGratuita(Reserva reserva) {
		return true;
		
	}
	
	public int contarDias(LocalDate inicio, LocalDate fin) {
		return Period.between(inicio,fin).getDays();

	}

}
