package src;

import java.time.LocalDate;

public class CancelacionIntermedia extends PoliticasDeCancelacion{
	
	
	private Reserva reserva;
	
	public CancelacionIntermedia(Reserva reserva){
		this.reserva.cancelarReserva();
		}
	

		public float cancelar(Reserva reserva) {
			if(this.esCancelacionGratuita(reserva)) {
				return 0;
			}
			return this.pagarLaMitadOTodaLa(reserva);
			
		}
		
		public boolean esCancelacionGratuita(Reserva reserva) {
			return this.contarDias(reserva.getInicio(), reserva.getFin()) > 20;
			
			
		}
		
		public float pagarLaMitadOTodaLa(Reserva reserva) {
			if(this.contarDias(LocalDate.now(),reserva.getInicio()) > 10){
			
				return reserva.getPrecioTotal()/ 2;
				
			}
				return reserva.getPrecioTotal();
		}

}
