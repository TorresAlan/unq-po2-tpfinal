package src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Sistema {
	
	private ArrayList<Usuario>usuarios;
	private ArrayList<Inmueble>publicaciones;
	private ArrayList<Reserva>reservas;
	private Set<String>tipoInmueble;
	private Set<String>servicios;
	private ArrayList<String>categoriaInquilino;
	private ArrayList<String>categoriaPropietario;
	private ArrayList<String>categoriaInmueble;
	
	public Sistema() {
		this.usuarios = new ArrayList<Usuario>();
		this.publicaciones = new ArrayList<Inmueble>();
		this.tipoInmueble = new HashSet<String>();
		this.servicios = new HashSet<String>();
		this.reservas = new ArrayList<Reserva>();
		this.categoriaInmueble = new ArrayList<String>();
		this.categoriaInquilino = new ArrayList<String>();
		this.categoriaPropietario = new ArrayList<String>();
	}

/**
 * 	Dado un nombre, un email, un telefono y una contraseņa, crea un nuevo usuario y lo registra en el sistema.
 */
	public void registrarUsuario(String nombre, String email, int telefono, String contrasenia) {
		Usuario usuario = new Usuario(nombre, email, telefono, contrasenia, this);
		
		this.usuarios.add(usuario);
	}
	
	public ArrayList<Usuario> getUsuarios(){
		return this.usuarios;
	}
	
/**
 * Dado un email y una contraseņa retorna el usuario con dichos datos.
 */
	public Usuario iniciarSesion(String email, String contrasenia) {
		for(Usuario usuario : this.usuarios) {
			if (usuario.getEmail() == email && usuario.getContrasenia() == contrasenia) {
				return usuario;
			}
		}
		throw new IllegalArgumentException("Usuario o contraseņa incorrectos");
	}
	
	public Set<String> getTipoInmueble(){
		return this.tipoInmueble;
	}
	
	public Set<String> getServicios(){
		return this.servicios;
	}
	
	public ArrayList<Inmueble> getPublicaciones(){
		return this.publicaciones;
	}
	
	public ArrayList<Reserva> getReservas(){
		return this.reservas;
	}
	
	protected ArrayList<String> getCategoriaInquilino(){
		return this.categoriaInquilino;
	}
	
	protected ArrayList<String> getCategoriaPropietario(){
		return this.categoriaPropietario;
	}
	
	protected ArrayList<String> getCategoriaInmueble(){
		return this.categoriaInmueble;
	}
	
	
	
	
	

}
