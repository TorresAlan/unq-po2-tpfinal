package src;

public class SinCancelacion extends PoliticasDeCancelacion {
	
	private Reserva reserva;
	
	public SinCancelacion(Reserva reserva){
		this.reserva = reserva;
	}
	
	public float cancelar(Reserva reserva) {
		return reserva.getPrecioTotal();		
	}
	

}
