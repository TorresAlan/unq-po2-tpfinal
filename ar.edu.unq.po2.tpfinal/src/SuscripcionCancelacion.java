package src;

/**
 * Interfaz para el observador de cancelacion de un inmueble.
 * @author AlanTorres
 *
 */

public interface SuscripcionCancelacion {
	
	public void updateCancelacion(String tipoInmueble);

}
