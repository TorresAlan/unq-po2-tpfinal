package src;

import java.time.LocalDate;
import java.util.ArrayList;

public class BuscarPrecionMinimo extends BuscarInmueble{
	
	private float precioMinimo;

	public BuscarPrecionMinimo(String ciudad, LocalDate inicio, LocalDate fin, ArrayList<Inmueble> publicaciones, float precioMinimo) {
		super(ciudad, inicio, fin, publicaciones);
		// TODO Auto-generated constructor stub
		this.precioMinimo = precioMinimo;
	}
	
	public ArrayList<Inmueble> buscar(){
		ArrayList<Inmueble> resultado= new ArrayList<>();
		this.getPublicaciones().parallelStream().filter(p -> !p.estaOcupado(inicio, fin) && this.esMismaCiudad(ciudad, p.getCiudad()) && this.esMayorA(this.getPrecioMinimo(), p.getPrecio()) ).forEach(j -> resultado.add(j));	
		return resultado;
	}
	
	private float getPrecioMinimo() {
		// TODO Auto-generated method stub
		return precioMinimo;
	}

	public boolean esMayorA(float num1, float num2) {
		return num1<num2;
		
		
	}

}
