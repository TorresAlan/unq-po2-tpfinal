package src;

import java.time.LocalDate;
import java.util.ArrayList;

public class Notificador {
	
	ArrayList<SuscripcionBajaPrecio>suscriptoresBajaPrecio;
	ArrayList<SuscripcionCancelacion>suscriptoresCancelacion;
	ArrayList<SuscripcionReserva>suscriptoresReserva;
	
	public Notificador () {
		this.suscriptoresBajaPrecio = new ArrayList<SuscripcionBajaPrecio>();
		this.suscriptoresCancelacion = new ArrayList<SuscripcionCancelacion>();
		this.suscriptoresReserva = new ArrayList<SuscripcionReserva>();
	}
	
	
/**
* Dado un suscriptor, lo agrega a la lista de suscriptores a recibir notificaciones a la baja de precio.
*/
	public void suscribirABajaDePrecio(SuscripcionBajaPrecio suscriptor) {
		if(!this.suscriptoresBajaPrecio.contains(suscriptor)) {
			this.suscriptoresBajaPrecio.add(suscriptor);
		}
	}
	
/**
* Dado un suscriptor, lo agrega a la lista de suscriptores a recibir notificaciones a la cancelacion de un inmueble
*/
	public void suscribirACancelacion(SuscripcionCancelacion suscriptor) {
		if(!this.suscriptoresCancelacion.contains(suscriptor)) {
			this.suscriptoresCancelacion.add(suscriptor);
		}
	}
	
/**
* Dado un suscriptor, lo agrega a la lista de suscriptores a recibir notificaciones a la reserva de un inmueble
*/
	public void suscribirAReserva(SuscripcionReserva suscriptor) {
		if(!this.suscriptoresReserva.contains(suscriptor)) {
			this.suscriptoresReserva.add(suscriptor);
		}
	}
		
/**
* Dado un suscriptor lo quita de la lista de suscriptores a recibir notificaciones a la baja de precio.
*/
	public void removerSuscripcionBajaDePrecio(SuscripcionBajaPrecio suscriptor) {
		this.suscriptoresBajaPrecio.remove(suscriptor);
	}
	
/**
* Dado un suscriptor lo quita de la lista de suscriptores a recibir notificaciones a la cancelacion de un inmueble.
*/
	public void removerSuscripcionCancelacion(SuscripcionCancelacion suscriptor) {
		this.suscriptoresCancelacion.remove(suscriptor);
	}
		
/**
* Dado un suscriptor lo quita de la lista de suscriptores a recibir notificaciones a la reserva de un inmueble
*/
	public void removerSuscripcionReserva(SuscripcionReserva suscriptor) {
		this.suscriptoresReserva.remove(suscriptor);
	}
		
/**
* Envia una notificacion a todos los suscriptores de baja de precio.
*/
	protected void notificarBajaDePrecio(String tipoInmueble, float precio) {
		for (SuscripcionBajaPrecio suscriptor : this.suscriptoresBajaPrecio) {
			suscriptor.updateBajaPrecio(tipoInmueble, precio);
		}
	}
	
/**
* Envia una notificacion a todos los suscriptores de cancelacion.
*/
	protected void notificarCancelacion(String tipoInmueble) {
		for (SuscripcionCancelacion suscriptor : this.suscriptoresCancelacion) {
			suscriptor.updateCancelacion(tipoInmueble);
		}
	}
	
/**
* Envia una notificacion a todos los suscriptores de reserva.
*/
	protected void notificarReserva(String tipoInmueble, LocalDate inicio, LocalDate fin) {
		for (SuscripcionReserva suscriptor : this.suscriptoresReserva) {
			suscriptor.updateReserva(tipoInmueble, inicio, fin);
		}
	}
	
	
	
	
	

}
