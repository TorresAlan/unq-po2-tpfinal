package src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ranking {
	
	private ArrayList<String> categorias;
	private Map<String, Float> promedioPorCategoria;
	private float promedioTotal;
	private ArrayList<String> comentarios;
	
	public Ranking(ArrayList<String>categorias) {
		this.categorias = categorias;
		this.promedioPorCategoria = new HashMap<String, Float>();
		this.comentarios = new ArrayList<String>();
	}

/**
 * 	Dada una categoria, una nota, un comentario y un usuario, realiza un rankeo
 * si la categoria es invalida envia un error.
 */
	public void rankear(String categoria, int nota, String comentario, Usuario usuario) {
		
		if (this.categorias.contains(categoria)) {
			this.agregarAPromedioPorCategoria(categoria, nota);
			this.agregarComentario(categoria, nota, comentario, usuario);
		}
		else {
			throw new IllegalArgumentException("Categoria no valida");
		}
	}

/**
 * Dada una categoria y una nota. calcula el nuevo promedio de la categoria dada y actualiza el promedio total.
 */
	private void agregarAPromedioPorCategoria(String categoria, float nota) {
		
		if (this.promedioPorCategoria.containsKey(categoria)) {
			this.promedioPorCategoria.replace(categoria, (this.promedioPorCategoria.get(categoria) + nota)/2);
		} else {
			this.promedioPorCategoria.put(categoria, nota);
		}
		this.actualizarPromedioTotal();
	}

/**
 * Actualiza el promedio total con los promedios de cada categoria.	
 */
	private void actualizarPromedioTotal() {
		
		float suma = 0;
		int cantidad = 0;
		
		for (String key : this.promedioPorCategoria.keySet()) {
			suma += this.promedioPorCategoria.get(key);
			cantidad++;
		}
		this.promedioTotal = suma / cantidad;
	}

/**
 * 	Agrega un nuevo comentario al ranking combinando la categoria, nota, nombre de usuario y comentario realizado por el Usuario.
 */
	private void agregarComentario(String categoria, int nota, String comentario, Usuario usuario) {
		
		String comentarioFormado = usuario.getNombre() + " califico en la categoria " + categoria + " con una nota de " + nota + " y comento:" + comentario;
		
		this.comentarios.add(comentarioFormado);
	}
	
	public float getPromedioTotal() {
		return this.promedioTotal;
	}

/**
 * 	Dada una categoria devuelve su promedio.
 */
	public float getPromedioDeCategoria(String categoria) {
		return this.promedioPorCategoria.get(categoria);
	}
	
	

}
