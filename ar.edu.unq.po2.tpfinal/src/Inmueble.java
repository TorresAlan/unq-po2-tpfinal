package src;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Inmueble {
	
	private String tipoInmueble;
	private float superficie;
	private String pais;
	private String ciudad;
	private String direccion;
	private ArrayList<String> servicios;
	private int capacidad;
	private ArrayList<String> fotos;
	private Usuario propietario;
	private LocalTime checkIn;
	private LocalTime checkOut;
	private ArrayList<String> formasDePago = new ArrayList<String>();
	private float precio;
	private ArrayList<PrecioTemporada> precioTemporada;
	private Notificador notificador;
	private Map<LocalDate, LocalDate> fechasDeOcupacion;
	private ArrayList<Reserva> reservas;
	private Ranking ranking;
	
	public Inmueble(String tipoInmueble,float superficie,String pais,String ciudad,String direccion,ArrayList<String>servicios,int capacidad,ArrayList<String>fotos, Usuario propietario, LocalTime checkIn, LocalTime checkOut, ArrayList<String> formasDePago, float precio) {
		
		super();
		this.tipoInmueble = tipoInmueble;
		this.superficie = superficie;
		this.pais = pais;
		this.ciudad = ciudad;
		this.direccion = direccion;
		this.servicios = servicios;
		this.capacidad = capacidad;
		this.fotos = fotos;
		this.propietario = propietario;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.formasDePago = formasDePago;
		this.precio = precio;
		this.precioTemporada = new ArrayList<PrecioTemporada>();
		this.fechasDeOcupacion = new HashMap<LocalDate,LocalDate>();
		this.reservas = new ArrayList<Reserva>();
		this.notificador = new Notificador();
		this.ranking = new Ranking(this.propietario.getSistema().getCategoriaInmueble());
		
	}
	
	public float getPrecio() {
		return this.precio;
	}
	
	public float getSuperficie() {
		return this.superficie;
	}
	
	public String getPais() {
		return this.pais;
	}
	
	public String getCiudad() {
		return this.ciudad;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
	
	public int getCapacidad() {
		return this.capacidad;
	}
	
	public ArrayList<String> getServicios() {
		return this.servicios;
	}
	
	public ArrayList<String> getFotos() {
		return this.fotos;
	}
	
	protected Map<LocalDate, LocalDate> getFechasDeOcupacion() {
		return this.fechasDeOcupacion;
	}

	public String getTipoInmueble() {
		return this.tipoInmueble;
	}
	
	public Usuario getPropietario() {
		return this.propietario;
	}
	
	public LocalTime getCheckIn() {
		return this.checkIn;
	}

	public LocalTime getCheckOut() {
		return this.checkOut;
	}

	public ArrayList<String> getFormasDePago() {
		return this.formasDePago;
	}
	
	public ArrayList<PrecioTemporada> getPrecioTemporada(){
		return this.precioTemporada;
	}
	
	public Notificador getNotificador() {
		return this.notificador;
	}
	
	protected ArrayList<Reserva> getReservas() {
		return this.reservas;
	}
	
	public Ranking getRanking() {
		return this.ranking;
	}
	

/**
 * Crea una temporada con precio y la agrega a la lista.
 */
	public void agregarPrecioTemporada(LocalDate inicio, LocalDate fin, float precio) {
		PrecioTemporada nuevaTemporada = new PrecioTemporada(inicio, fin, precio);
		this.getPrecioTemporada().add(nuevaTemporada);
		
	}

/**
 * Dada una fecha de inicio y una de fin, calcula el precio de alquiler del inmueble.
 */
	public float precioDeHasta(LocalDate inicio, LocalDate fin) {
		float precio = 0;
		List<LocalDate>dias = inicio.datesUntil(fin.plusDays(1)).toList();
		
		for(LocalDate dia : dias){
		    precio = precio + this.precioSegunTemporada(dia);
		}
		
		return precio;
	}

/**
 * Dado una fecha, devuelve el precio si pertenece a una temporada. De no pertenecer devuelve el precio default.
 */
	private float precioSegunTemporada(LocalDate fecha) {
		for (PrecioTemporada temporada : this.precioTemporada) {
			if(temporada.perteneceFecha(fecha)) {
				return temporada.getPrecio();
			}
		}
 		return this.precio;
	}
	
	
/**
 * Setea un nuevo precio default. Si es menor al precio actual envia notificaciones.
 */
	public void cambiarPrecio(float precio) {
		float precioAnterior = this.precio;
		this.precio = precio;
		if (this.precio < precioAnterior) {
			this.notificador.notificarBajaDePrecio(this.tipoInmueble, this.precio);
		}
	}

/**
 * Dadas una fecha de inicio y una de fin, devuelve true si el inmueble esta ocupado entre esas fechas.
 */
	public boolean estaOcupado(LocalDate inicio, LocalDate fin) {
		for(LocalDate fechaIni : this.fechasDeOcupacion.keySet()) {
			if (fechaIni.isBefore(inicio) && this.fechasDeOcupacion.get(fechaIni).isAfter(fin)) {
				return true;
			}
		}
		return false;
	}

/**
 * 	Dadas una fecha de inicio y una de fin, devuelve una reserva realizada entre esas fechas.
 *  De no encontrar ninguna devuelve un error.
 */
	protected Reserva buscarReserva(LocalDate inicio, LocalDate fin) {
		
		for(Reserva reserva : this.reservas) {
			if((reserva.getInicio().isBefore(inicio) || reserva.getInicio().isEqual(inicio)) && (reserva.getFin().isAfter(fin) || reserva.getFin().isEqual(fin))){
				return reserva;
			}
		}
		throw new IllegalArgumentException("No existe la reserva buscada");
	}
	
	


}
