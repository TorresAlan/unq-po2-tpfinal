package src;

public class ReservaPendiente implements EstadoDeReserva {
	
	private Reserva reserva;
	
	public ReservaPendiente(Reserva reserva) {
		this.reserva = reserva;
	}

	/**
	 * Cambia el estado de la reserva a aceptada y la registra en el sistema si el metodo lo manda el propietario
	 * si otro usuario utiliza el metodo devuelve un error.
	 * En caso de ser aceptada notifica a los suscriptores de reserva.
	 */
	@Override
	public void aceptarReserva(Usuario usuario) {
		if (usuario == this.reserva.getInmueble().getPropietario()) {
			reserva.getInquilino().getSistema().getReservas().add(reserva);
			reserva.getInmueble().getPropietario().getSolicitudesDeReserva().remove(reserva);
			reserva.getInmueble().getReservas().add(reserva);
			reserva.getInmueble().getFechasDeOcupacion().put(reserva.getInicio(), reserva.getFin());
			reserva.setEstado(new ReservaAceptada(this.reserva));
			this.reserva.getInmueble().getNotificador().notificarReserva(this.reserva.getInmueble().getTipoInmueble(), this.reserva.getInicio(), this.reserva.getFin());
		}
		else {
			throw new IllegalArgumentException("Solo el propietario puede aceptar esta reserva");
		}
	}

/**
 * Cancela la reserva quitandola de las solicitudes del propietario y de las reservas realizadas por el inquilino.
 */
	@Override
	public void cancelarReserva() {
		reserva.getInmueble().getPropietario().getSolicitudesDeReserva().remove(reserva);
		reserva.getInquilino().getMisReservas().remove(reserva);
	}

	
	
	
}
