package src;

import java.time.LocalDate;
import java.time.Period;

public class CancelacionMinima extends PoliticasDeCancelacion {
	
	private Reserva reserva;
	
	public CancelacionMinima(Reserva reserva){
		this.reserva.cancelarReserva();
		
	}
	

		public float cancelar(Reserva reserva) {
			if(this.esCancelacionGratuita(reserva)) {
				return 0;
			}
			return reserva.getPrecioTotal()/ this.contarDias(LocalDate.now(), reserva.getInicio())*2;
			
		}
		
		public boolean esCancelacionGratuita(Reserva reserva) {
			return this.contarDias(reserva.getInicio(), reserva.getFin()) > 10;
			
			
		}
		

		
		
}
