package test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Inmueble;
import src.Notificador;
import src.Sistema;
import src.SuscripcionBajaPrecio;
import src.SuscripcionCancelacion;
import src.SuscripcionReserva;
import src.Usuario;

class NotificadorTestCase {
	
	Notificador notificador; //SUT
	
	Administrador administrador; //DOC
	Sistema sistema; //DOC
	SuscripcionBajaPrecio susBajaPrecio; //DOC
	SuscripcionCancelacion susCancelacion; //DOC
	SuscripcionReserva susReserva; //DOC
	Inmueble inmueble; //DOC
	Usuario inquilino; //DOC
	Usuario propietario; //DOC
	
	@BeforeEach
	void setUp() {
		sistema = new Sistema();
		administrador = new Administrador(sistema);
		
		sistema.registrarUsuario("AAA", "aaa", 123, "123");
		sistema.registrarUsuario("BBB", "bbb", 456, "456");
		
		propietario = sistema.iniciarSesion("aaa", "123");
		inquilino = sistema.iniciarSesion("bbb", "456");
		
		susBajaPrecio = mock(SuscripcionBajaPrecio.class);
		susCancelacion = mock(SuscripcionCancelacion.class);
		susReserva = mock(SuscripcionReserva.class);
		
		administrador.darDeAltaServicios("Agua");
		administrador.darDeAltaTipoDeInmueble("Departamento");
		
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua"));
		ArrayList<String>formasDePago = new ArrayList<String>(Arrays.asList("Efectivo"));
		
		propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, LocalTime.of(8, 0), LocalTime.of(15, 0), formasDePago, 50);
		
		inmueble = propietario.getMisInmueblesPublicados().get(0);
	}

/**
* Verifica que los suscriptores a la baja de precio reciban las notificaciones.
*/
	@Test
	void testRecibeMensajeSuscripcionBajaPrecio() {
		inmueble.getNotificador().suscribirABajaDePrecio(susBajaPrecio);
		inmueble.cambiarPrecio(40);
			
		verify(susBajaPrecio, times(1)).updateBajaPrecio("Departamento", 40);
	}


/**
*Verifica que la baja a la suscripcion de baja precio funcione correctamente.
*/
	@Test
	void testUnSuscriptorSeDaDeBajaABajaPrecio() {
		inmueble.getNotificador().suscribirABajaDePrecio(susBajaPrecio);
		inmueble.getNotificador().removerSuscripcionBajaDePrecio(susBajaPrecio);
			
		inmueble.cambiarPrecio(40);
			
		verify(susBajaPrecio, times(0)).updateBajaPrecio("Departamento", 40);
			
	}

/**
* Verifica que no se envien notificaciones en caso de que el precio suba.
*/
	@Test
	void testNoSeEnvianNotificacionesSiElPrecioSube() {
		inmueble.getNotificador().suscribirABajaDePrecio(susBajaPrecio);
			
		inmueble.cambiarPrecio(100);
			
		verify(susBajaPrecio, times(0)).updateBajaPrecio("Departamento", 100);
	}

/**
 * Verifica que los suscriptores de cancelacion reciban los updates correctamente.
 */
	@Test
	void testRecibenMensajeSuscripcionCancelacion() {
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
		inmueble.getNotificador().suscribirACancelacion(susCancelacion);
		
		inquilino.getMisReservas().get(0).cancelarReserva();
		
		verify(susCancelacion, times(1)).updateCancelacion("Departamento");
	}
	
/**
 * Verifica que los suscriptores de reserva reciban los updates correctamente.	
 */
	@Test
	void testRecibenMensajeSuscripcionReserva() {
		inmueble.getNotificador().suscribirAReserva(susReserva);
		
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
		verify(susReserva, times(1)).updateReserva("Departamento", LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15));
		
	}

/**
*Verifica que la baja a la suscripcion de cancelacion funcione correctamente.
*/	
	@Test
	void testUnSuscriptorSeDaDeBajaACancelacion() {
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
		inmueble.getNotificador().suscribirACancelacion(susCancelacion);
		inmueble.getNotificador().removerSuscripcionCancelacion(susCancelacion);
		
		inquilino.getMisReservas().get(0).cancelarReserva();
		
		verify(susCancelacion, times(0)).updateCancelacion("Departamento");
			
	}
	
/**
*Verifica que la baja a la suscripcion de reserva funcione correctamente.
*/	
	@Test
	void testUnSuscriptorSeDaDeBajaAReserva() {
		inmueble.getNotificador().suscribirAReserva(susReserva);
		inmueble.getNotificador().removerSuscripcionReserva(susReserva);
		
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
		verify(susReserva, times(0)).updateReserva("Departamento", LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15));
			
	}
	
	
	
	
}
