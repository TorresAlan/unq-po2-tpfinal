package test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.HomePagePublisher;
import src.HomePagePublisherAdapter;
import src.Inmueble;
import src.Sistema;
import src.Usuario;

class HomePagePublisherAdapterTestCase {
	
	private HomePagePublisherAdapter adapter; //SUT
	
	private HomePagePublisher paginaTipoTrivago; //DOC
	private Inmueble publicacion; //DOC
	private Sistema sistema;
	private Usuario propietario;
	
	@BeforeEach
	void setUp() {
		
		sistema = new Sistema();
		
		sistema.registrarUsuario("AAA", "aaa", 123, "123");
		
		propietario = sistema.iniciarSesion("aaa", "123");
		
		paginaTipoTrivago = mock(HomePagePublisher.class);
		
		publicacion = spy(new Inmueble("Departamento", 0, null, null, null, null, 0, null, propietario, null, null, null, 100));
		
		
		adapter = new HomePagePublisherAdapter(paginaTipoTrivago);
	}

/**
 * Verifica la correcta ejecicion de la notificacion adaptada.
 */
	@Test
	void testAdaptadorEnviaRespuestaAdaptadaMedianteUnaNotificacion() {
		
		publicacion.getNotificador().suscribirABajaDePrecio(adapter);
		publicacion.cambiarPrecio(50);
		
		verify(paginaTipoTrivago, times(1)).publish("No te pierdas esta oferta: Un inmueble Departamento a tan solo 50.0 pesos");
	}

}
