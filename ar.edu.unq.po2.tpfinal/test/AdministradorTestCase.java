package test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Sistema;

class AdministradorTestCase {
	
	private Administrador administrador; //SUT
	
	private Sistema sistema; //DOC

	@BeforeEach
	void setUp() {
		sistema = spy(Sistema.class);
		
		administrador = new Administrador(sistema);
	}

/**
 * Verifica si se da de alta correctamente un tipo de inmueble
 */
	@Test
	void testAgregarNuevoTipoInmueble() {
		administrador.darDeAltaTipoDeInmueble("Departamento");
		
		assertTrue(sistema.getTipoInmueble().contains("Departamento"));
	}
	
/**
 * Verifica si se da de alta correctamente un servicio.
 */
	@Test
	void testAgregarNuevoServicio() {
		administrador.darDeAltaServicios("Gas");
		
		assertTrue(sistema.getServicios().contains("Gas"));
	}

}
