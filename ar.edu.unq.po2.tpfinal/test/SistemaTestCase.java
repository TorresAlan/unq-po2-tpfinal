package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Sistema;
import src.Usuario;

class SistemaTestCase {
	
	private Sistema sistema; //SUT
	
	@BeforeEach
	void setUp() {
		sistema = new Sistema();
		
		sistema.registrarUsuario("Alan", "alan@gmail.com", 12345678, "1234");
	}

/**
 * Verifica el registro de un nuevo usuario en el sistema.
 */
	@Test
	void testRegistroDeNuevoUsuario() {
		
		assertEquals(1, sistema.getUsuarios().size());
	}

/**
 * Verifica el correcto acceso a un usuario registrado en el sistema.	
 */
	@Test
	void testInicioSesionValido() {
		
		Usuario usuario = sistema.iniciarSesion("alan@gmail.com", "1234");
		
		assertEquals("Alan", usuario.getNombre());
		assertEquals("alan@gmail.com", usuario.getEmail());
		assertEquals(12345678, usuario.getTelefono());
		assertEquals("1234", usuario.getContrasenia());
	}

/**
 * Verifica un inicio de sesion no valido.
 */
	@Test
	void testInicioSesionNoValido() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			sistema.iniciarSesion("asasas@gmail.com", "12345");
	    });
		
		String mensajeError = "Usuario o contraseņa incorrectos";
		String mensajeActual = exception.getMessage();
		
		assertTrue(mensajeActual.contains(mensajeError));
	}

}
