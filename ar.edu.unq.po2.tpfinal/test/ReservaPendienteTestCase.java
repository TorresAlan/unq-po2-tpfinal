package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Inmueble;
import src.Reserva;
import src.ReservaPendiente;
import src.Sistema;
import src.Usuario;

class ReservaPendienteTestCase {
	
	ReservaPendiente reservaPendiente; //SUT
	
	Reserva reserva; //DOC
	
	Sistema sistema; //DOC
	Administrador admin; //DOC
	Inmueble inmueble; //DOC
	Usuario inquilino; //DOC
	Usuario propietario; //DOC
	
	@BeforeEach
	void setUp() {
		sistema = new Sistema();
		admin = new Administrador(sistema);
		
		sistema.registrarUsuario("AAA", "aaa", 123, "123");
		sistema.registrarUsuario("BBB", "bbb", 456, "456");
		
		propietario = sistema.iniciarSesion("aaa", "123");
		inquilino = sistema.iniciarSesion("bbb", "456");
		
		admin.darDeAltaServicios("Agua");
		admin.darDeAltaTipoDeInmueble("Departamento");
		
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua"));
		ArrayList<String>formasDePago = new ArrayList<String>(Arrays.asList("Efectivo"));
		
		propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, LocalTime.of(8, 0), LocalTime.of(15, 0), formasDePago, 50);
		
		inmueble = propietario.getMisInmueblesPublicados().get(0);
		
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		
		reserva = inquilino.getMisReservas().get(0);
		
	}

/**
 * Comprueba que al ser aceptada una reserva por el propietario esta queda registrada en el sistema.	
 */
	@Test
	void testPropietarioAceptaReserva() {
		
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
		assertEquals(reserva, sistema.getReservas().get(0));
	}
	
/**
 * Comprueba que se reciba el error esperado si alguien que no es el propietario intenta aceptar la reserva.
 */
	@Test
	void testOtroUsuarioQueNoEsElPropietarioAceptaReserva() {
		
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {reserva.aceptarReserva(inquilino);});
		
		String mensajeError = "Solo el propietario puede aceptar esta reserva";
		String mensajeActual = exception.getMessage();
		
		assertTrue(mensajeActual.contains(mensajeError));
		
	}

/**
 * Comprueba que al cancelar una reserva esta sea eliminada de las solicitudes del propietario y de las reservas del inquilino.
 */
	@Test
	void cancelarReservaPendiente() {
		
		propietario.getSolicitudesDeReserva().get(0).cancelarReserva();
		
		assertEquals(0, propietario.getSolicitudesDeReserva().size());
		assertEquals(0, inquilino.getMisReservas().size());
		
	}

}
