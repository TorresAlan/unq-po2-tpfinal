package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Inmueble;
import src.Reserva;
import src.Sistema;
import src.Usuario;

class RankingTestCase {
	
//	private Ranking ranking; //SUT
	
	private Administrador administrador; //DOC
	private Sistema sistema; //DOC
	private Inmueble inmueble; //DOC
	private Usuario inquilino; //DOC
	private Usuario propietario; //DOC
	
	@BeforeEach
	void setUp() {
	sistema = new Sistema();
	administrador = new Administrador(sistema);
	
	sistema.registrarUsuario("AAA", "aaa", 123, "123");
	sistema.registrarUsuario("BBB", "bbb", 456, "456");
	
	propietario = sistema.iniciarSesion("aaa", "123");
	inquilino = sistema.iniciarSesion("bbb", "456");
	
	administrador.darDeAltaServicios("Agua");
	administrador.darDeAltaTipoDeInmueble("Departamento");
	
	administrador.darDeAltaCategoriaInmueble("Estado");
	administrador.darDeAltaCategoriaInquilino("Trato");
	administrador.darDeAltaCategoriaPropietario("Atencion");
	
	ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua"));
	ArrayList<String>formasDePago = new ArrayList<String>(Arrays.asList("Efectivo"));
	
	propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, LocalTime.of(8, 0), LocalTime.of(15, 0), formasDePago, 50);
	
	inmueble = propietario.getMisInmueblesPublicados().get(0);
	
	inquilino.realizarReserva(inmueble, LocalDate.of(2020, 6, 15), LocalDate.of(2020, 6, 20), "Efectivo", "despue vemo");
	propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
	}

/**
 * Hace un rankeo en cada ranking y comprueba que se hagan correctamente.
 */
	@Test
	void testRankeosValidos() {
		
		Reserva reserva = inquilino.getMisReservas().get(0);
		inquilino.rankear(reserva,reserva.getInmueble().getPropietario().getRankingPropietario() , "Atencion", 5, "Muy buena atencion.");
		
		inquilino.rankear(reserva, reserva.getInmueble().getRanking(), "Estado", 4, "Muy bien cuidado");
		
		propietario.rankear(reserva, reserva.getInquilino().getRankingInquilino(), "Trato", 1, "Me trato muy mal");
		
		assertEquals(5, propietario.getRankingPropietario().getPromedioDeCategoria("Atencion"));
		assertEquals(4, inmueble.getRanking().getPromedioDeCategoria("Estado"));
		assertEquals(1, inquilino.getRankingInquilino().getPromedioDeCategoria("Trato"));
	}

/**
 * Comprueba el mensaje de error que se envia si se intenta hacer un rankeo con una categoria no valida.
 */
	@Test
	void testRankeoInvalido() {
		Reserva reserva = inquilino.getMisReservas().get(0);
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {inquilino.rankear(reserva, reserva.getInmueble().getRanking(), "Servicios", 4, "Muy bien cuidado");});
		
		String mensajeError = "Categoria no valida";
		String mensajeActual = exception.getMessage();
		
		assertTrue(mensajeActual.contains(mensajeError));
	}

/**
 * Comprueba que el calculo del promedio total se haga correctamente.	
 */
	@Test
	void testCalculoPromedioTotal() {
		Reserva reserva = inquilino.getMisReservas().get(0);
		inquilino.rankear(reserva, reserva.getInmueble().getRanking(), "Estado", 5, "Muy bien cuidado");
		inquilino.rankear(reserva, reserva.getInmueble().getRanking(), "Estado", 1, "Muy mal cuidado");
		
		assertEquals(3, reserva.getInmueble().getRanking().getPromedioTotal());
	}
	
	

}
