package test;


import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.PrecioTemporada;

class PrecioTemporadaTestCase {
	
	private PrecioTemporada temporada;
	
	private LocalDate inicio;
	private LocalDate fin;
	
	@BeforeEach
	void setUp() {
		
		inicio = LocalDate.of(2020, 7, 10);
		fin = LocalDate.of(2020, 7, 15);
		
		temporada = new PrecioTemporada(inicio, fin, 100);
		

	}

/**
 * Verifica el funcionamiento correcto de una fecha entre el rango de la temporada.	
 */
	@Test
	void testFechaEntreQuePertenece() {
		LocalDate fecha = LocalDate.of(2020, 7, 11);
		
		assertTrue(temporada.perteneceFecha(fecha));
	}

/**
 * Verifica que se devuelva false si se pasa como parametro un fecha que no pertenezca al rango de la temporada.	
 */
	@Test
	void testFechaNoPertenece() {
		LocalDate fecha = LocalDate.of(2020, 6, 11);
		
		assertFalse(temporada.perteneceFecha(fecha));
	}

/**
 * Verifica que se devuelva true si se pasa como parametro una fecha igual a la fecha de inicio o fin de la temporada.	
 */
	@Test
	void testFechaIgualInicio() {
		LocalDate fecha = LocalDate.of(2020, 7, 10);
		
		assertTrue(temporada.perteneceFecha(fecha));
	}

}
