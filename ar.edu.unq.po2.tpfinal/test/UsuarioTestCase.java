package test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Sistema;
import src.Usuario;

class UsuarioTestCase {
	
	private Usuario propietario; //SUT
	private Usuario inquilino;
	
	private Sistema sistema; //DOC
	private Administrador administrador; //DOC
	
	@BeforeEach
	private void setUp() {
		sistema = spy(Sistema.class);
		administrador = spy(new Administrador(sistema));
		
		sistema.registrarUsuario("Alan", "alan@hotmail.com", 12345678, "1234");
		sistema.registrarUsuario("Manolo", "manolo@gmail.com", 555222555, "0000");
		
		propietario = sistema.iniciarSesion("alan@hotmail.com", "1234");
		inquilino = sistema.iniciarSesion("manolo@gmail.com", "0000");
		
		administrador.darDeAltaServicios("Agua");
		administrador.darDeAltaTipoDeInmueble("Departamento");
		
		
	}

/**
 * Verifica la creacion de un inmueble y que se registre en el sistema.
 */
	@Test
	void testPublicarInmuebleValido() {
		
		administrador.darDeAltaServicios("Gas");

		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua", "Gas"));
		
		propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, null, null, null, 50);
		
		assertEquals(1, sistema.getPublicaciones().size());
		
	}

/**
 * Verifica que se retorne un error si se intenta crear un inmueble que contenga servicios o un tipo de inmueble no valido para el sistema.
 */
	@Test
	void testPublicarInmuebleInvalido() {
		
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua", "Gas"));
		
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, null, null, null, 50);;
	    });
		
		String mensajeError = "Tipo de inmueble o servicio no valido";
		String mensajeActual = exception.getMessage();
		
		assertTrue(mensajeActual.contains(mensajeError));
	}
	
/**
 * Verifica la realizacion valida de una reserva
 */
	@Test
	void realizarReservaValida() {
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua"));
		ArrayList<String>formasDePago = new ArrayList<String>(Arrays.asList("Efectivo"));
		
		propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, LocalTime.of(8, 0), LocalTime.of(15, 0), formasDePago, 50);
		
		inquilino.realizarReserva(sistema.getPublicaciones().get(0), LocalDate.of(2020, 1, 10), LocalDate.of(2020, 1, 15), "Efectivo", "Nos pondremos de acuerdo via email");
		
		assertEquals(1, inquilino.getMisReservas().size());
		assertEquals(1, propietario.getSolicitudesDeReserva().size());
	}

}
