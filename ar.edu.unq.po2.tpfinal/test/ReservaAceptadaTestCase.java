package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Administrador;
import src.Inmueble;
import src.Reserva;
import src.ReservaPendiente;
import src.Sistema;
import src.Usuario;

class ReservaAceptadaTestCase {
	
	ReservaPendiente reservaPendiente; //SUT
	
	Reserva reserva; //DOC
	
	Sistema sistema; //DOC
	Administrador admin; //DOC
	Inmueble inmueble; //DOC
	Usuario inquilino; //DOC
	Usuario propietario; //DOC
	
	@BeforeEach
	void setUp() {
		sistema = new Sistema();
		admin = new Administrador(sistema);
		
		sistema.registrarUsuario("AAA", "aaa", 123, "123");
		sistema.registrarUsuario("BBB", "bbb", 456, "456");
		
		propietario = sistema.iniciarSesion("aaa", "123");
		inquilino = sistema.iniciarSesion("bbb", "456");
		
		admin.darDeAltaServicios("Agua");
		admin.darDeAltaTipoDeInmueble("Departamento");
		
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Agua"));
		ArrayList<String>formasDePago = new ArrayList<String>(Arrays.asList("Efectivo"));
		
		propietario.publicarInmueble("Departamento", 20, "Argentina", "Berazategui", "1234", servicios, 2, null, LocalTime.of(8, 0), LocalTime.of(15, 0), formasDePago, 50);
		
		inmueble = propietario.getMisInmueblesPublicados().get(0);
		
		inquilino.realizarReserva(inmueble, LocalDate.of(2020, 4, 9), LocalDate.of(2020, 4, 15), "Efectivo", "despue vemo");
		
		reserva = inquilino.getMisReservas().get(0);
		
		propietario.getSolicitudesDeReserva().get(0).aceptarReserva(propietario);
		
	}

/**
 * Comprueba el mensaje de error al intentar aceptar una reserva ya aceptada.
 */
	@Test
	void testAceptarReservaYaAceptada() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {reserva.aceptarReserva(propietario);});
		
		String mensajeError = "Esta reserva ya a sido aceptada";
		String mensajeActual = exception.getMessage();
		
		assertTrue(mensajeActual.contains(mensajeError));
	}

/**
 * Comprueba que al cancelar una reserva aceptada estas queden fuera de las reservas del inquilino y del sistema.
 */
	@Test
	void testCancelarReservaAceptada() {
		inquilino.getMisReservas().get(0).cancelarReserva();
		
		assertEquals(0, inquilino.getMisReservas().size());
		assertEquals(0, sistema.getReservas().size());
	}

/**
 * Verifica que se realice una reserva condicional al cancelar la reserva.	
 */
	@Test
	void testSeRealizaReservaCondicionalAlCancelar() {
		sistema.registrarUsuario("inquilino condicional", "unEmail", 23, "unaContraseņa");
		Usuario inquilinoCondicional = sistema.iniciarSesion("unEmail", "unaContraseņa");
		
		inquilinoCondicional.realizarReservaCondicional(inmueble, LocalDate.of(2020, 4, 10), LocalDate.of(2020, 4, 14), "Efectivo", "Vemo");
		
		inquilino.getMisReservas().get(0).cancelarReserva();
		
		assertEquals(inquilinoCondicional, propietario.getSolicitudesDeReserva().get(0).getInquilino());
	}

	
	
}
