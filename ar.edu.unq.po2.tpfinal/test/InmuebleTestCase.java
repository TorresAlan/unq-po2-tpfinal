package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.Inmueble;
import src.Sistema;
import src.Usuario;

class InmuebleTestCase {
	
	private Inmueble inmueble; //SUT
	
	private Usuario propietario; //DOC
	private Sistema sistema; //DOC
	
	private LocalTime checkIn;
	private LocalTime checkOut;
	ArrayList<String> formasDePago;
	
	@BeforeEach
	void setUp() {
		sistema = new Sistema();
		
		sistema.registrarUsuario("AAA", "aaa", 123, "123");
		
		propietario = sistema.iniciarSesion("aaa", "123");
			
		checkIn = LocalTime.of(7, 00);
		checkOut = LocalTime.of(20, 00);
		formasDePago = new ArrayList<String>(Arrays.asList("Efectivo", "Tarjeta"));
		ArrayList<String>servicios = new ArrayList<String>(Arrays.asList("Luz", "Gas"));
		
		inmueble = new Inmueble("Departamento",25,"Argentina","Buenos Aires","1234",servicios,5,null, propietario, checkIn, checkOut, formasDePago, 50);
		}

/**
 * Verifica que una nueva temporada con precio fue agregada correctamente.
 */
	@Test
	void testAgregarUnNuevoPrecioTemporada() {
		LocalDate inicio = LocalDate.of(2020, 7, 10);
		LocalDate fin = LocalDate.of(2020, 7, 15);
		inmueble.agregarPrecioTemporada(inicio, fin, 80);

		assertEquals(inmueble.getPrecioTemporada().size(), 1);
		}

/**
 * Verifica el calculo del precio en un intervalo de dias pertenecientes todos a una misma temporada.
 */
	@Test
	void testComprobarPrecioConFechasEnUnaTemporada() {
		LocalDate inicio = LocalDate.of(2020, 7, 10);
		LocalDate fin = LocalDate.of(2020, 7, 15);
		inmueble.agregarPrecioTemporada(inicio, fin, 80);
		
		assertEquals(480, inmueble.precioDeHasta(inicio,fin));
	}

/**
 * Verifica el calculo del precio en un intervalo de dias en los que ninguno pertenece a una temporada.
 */
	@Test
	void testComprobarPrecioConFechasSinTemporada() {
		LocalDate inicio = LocalDate.of(2020, 7, 10);
		LocalDate fin = LocalDate.of(2020, 7, 15);
		inmueble.agregarPrecioTemporada(inicio, fin, 80);
		
		assertEquals(500, inmueble.precioDeHasta(LocalDate.of(2020, 3, 1),LocalDate.of(2020, 3, 10)));
	}

/**
 * Verifica el calculo del precio en un intervalo de dias con algunos pertenecientes a una temporada y otros no.
 */
	@Test
	void testComprobarPrecioConFechasConYSinTemporada() {
		LocalDate inicio = LocalDate.of(2020, 7, 10);
		LocalDate fin = LocalDate.of(2020, 7, 15);
		inmueble.agregarPrecioTemporada(inicio, fin, 80);
		
		assertEquals(260, inmueble.precioDeHasta(LocalDate.of(2020, 7, 14),LocalDate.of(2020, 7, 17)));
	}
	
	
}
